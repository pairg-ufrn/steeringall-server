sta_globals = {}
matches = []
locked_matches = False

def get_match(match_id):
    global matches
    return matches[(match_id - 1)]

def get_and_lock_matches():
    global locked_matches
    global matches
    while locked_matches:
        continue

    locked_matches = True
    return matches

def release_matches():
    global locked_matches
    locked_matches = False

def publish_match(match):
    global matches
    matches.append(match)
    match_id = len(matches)
    match.id = match_id
