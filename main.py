import socket
from staruntime import sta_globals
from staruntime.config import cfg as configuration
from src.boot import boot_tcp_server, boot_udp_server, boot_command_mode
from src.utils import log_debug

def stop_tcp_server(bind_ip, tcp_port):
    tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_client.connect((bind_ip, tcp_port))
    tcp_client.send('joker')
    tcp_client.close()

def stop_udp_server(bind_ip, udp_port):
    udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_client.sendto('joker', (bind_ip, udp_port))
    udp_client.close()

def stop_server(bind_ip, tcp_port, udp_port):
    sta_globals['tcp_run'] = False
    sta_globals['udp_run'] = False

    print ' - Sending TCP shutdown signal'
    stop_tcp_server(bind_ip, tcp_port)
    print ' - Done'

    print

    print ' - Sending UDP shutdown signal'
    stop_udp_server(bind_ip, udp_port)
    print ' - Done'


def start_server(server_host, server_tcp_port, server_udp_port):
    print " - Steering All Server - Version %s" % configuration.get_config('VERSION')
    print

    print " - Booting TCP server at %s:%d" % (server_host, server_tcp_port)
    boot_tcp_server(server_host, server_tcp_port)
    print " - Done"

    print

    print " - Booting UDP server at %s:%d" % (server_host, server_udp_port)
    boot_udp_server(server_host, server_udp_port)
    print " - Done"

    print

    sta_globals['server_started'] = True
    print " - Server started. Entering command mode"
    boot_command_mode()
    print " - Shutting down server's main thread"
    stop_server(server_host, server_tcp_port, server_udp_port)


if __name__ == '__main__':
    server_host = configuration.get_file_config().get('SERVER', 'host_name')
    server_tcp_port = configuration.get_file_config().getint('SERVER', 'tcp_port')
    server_udp_port = configuration.get_file_config().getint('SERVER', 'udp_port')
    sta_globals.update({'server_started': False})
    # try:
    start_server(server_host, server_tcp_port, server_udp_port)
    # except Exception:
    #     print "!! An error has ocurred, shutting down !!"
    #     if sta_globals['server_started']:
    #         stop_server(server_host, server_tcp_port, server_udp_port)
