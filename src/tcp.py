import socket
import threading
from staruntime import sta_globals
from src.request.tcp_handlers import HelloWorldHandler, StartMatchHandler
from src.utils import log_debug

def get_tcp_handlers():
    return [
        HelloWorldHandler(),
        StartMatchHandler()
    ]

def tcp_server_socket(bind_ip, bind_port):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((bind_ip, bind_port))
    server.listen(5)

    return server

def tcp_thread_target(tcp_socket, ligado=True, loko=False):
    while sta_globals['tcp_run']:
        client, addr = tcp_socket.accept()
        log_debug("[TCP] Accepted connection from %s:%d" % (addr[0], addr[1]), prefix='tcp')
        client_handler = threading.Thread(target=handle_client, args=(client,))
        client_handler.start()

    log_debug('[TCP] Shutting down TCP Server', prefix='tcp')
    tcp_socket.close()

def handle_client(client_socket):
    request = client_socket.recv(1024)
    parsed_request = request.split('_')

    handlers = get_tcp_handlers()
    for handler in handlers:
        response = handler.handle(parsed_request)
        if response:
            log_debug("[TCP] Sending '%s' as response" % response, prefix='tcp')
            client_socket.send(response)

    log_debug('[TCP] Shutting down TCP Connection', prefix='tcp')
    client_socket.close()
