from src.request import RequestHandler
from src.game import StaMatch
from staruntime import get_and_lock_matches, release_matches, publish_match, get_match


class HelloWorldHandler(RequestHandler):

    def __init__(self):
        super(HelloWorldHandler, self).__init__('hello')

    def handle_request(self, request, data):
        return "Hello my friend %s" % request[1]


class StartMatchHandler(RequestHandler):

    def __init__(self):
        super(StartMatchHandler, self).__init__('match')

    def handle_request(self, request, data):
        matches = get_and_lock_matches()
        match_found = False
        player_id = None
        match_id = None
        for match in matches:
            if not match.is_enabled():
                match_found = True
                player_id = match.connect()
                match_id = match.id
                release_matches()

        if not match_found:
            my_match = StaMatch()
            player_id = my_match.connect()
            publish_match(my_match)
            match_id = my_match.id
            release_matches()
            while not my_match.is_enabled():
                my_match = get_match(match_id)

        return "%d;%d" % (match_id, player_id)
