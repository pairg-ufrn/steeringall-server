import threading
from staruntime import sta_globals
from src.tcp import tcp_server_socket, tcp_thread_target
from src.udp import udp_server_socket, udp_thread_target
from staruntime.config import cfg
from src.command import StopServerCommand, QueryMatchesCommand, MatchInfosCommand

def get_commands():
    return [
        StopServerCommand(),
        QueryMatchesCommand(),
        MatchInfosCommand()
    ]

def boot_tcp_server(bind_ip, bind_port):
    tcp_socket = tcp_server_socket(bind_ip, bind_port)
    sta_globals.update({'tcp_run': True})
    tcp_thread = threading.Thread(target=tcp_thread_target, args=(tcp_socket,))
    tcp_thread.start()

def boot_udp_server(bind_ip, bind_port):
    udp_socket = udp_server_socket(bind_ip, bind_port)
    sta_globals.update({'udp_run': True})
    udp_thread = threading.Thread(target=udp_thread_target, args=(udp_socket,))
    udp_thread.start()

def boot_command_mode():
    sta_globals.update({'command_run': True})
    while sta_globals['command_run']:
        input = raw_input('Enter server command: > ')
        split_input = input.split(' ')
        commands = get_commands()
        for command in commands:
            command.call_command(split_input[0], split_input)
