from staruntime.config import cfg

def log_debug(message, prefix=None):
    should_print = True
    debug_global = cfg.get_file_config().getboolean('DEBUG', 'log')
    if not debug_global:
        should_print = False

    if prefix is not None and should_print:
        custom_debug = cfg.get_file_config().getboolean('DEBUG', prefix + '_log')
        if not custom_debug:
            should_print = False

    if should_print:
        print message
