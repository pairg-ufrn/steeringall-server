import socket
from staruntime import sta_globals, get_and_lock_matches, release_matches
from src.utils import log_debug

def get_udp_handlers():
    return []

def udp_server_socket(bind_ip, bind_port):
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((bind_ip, bind_port))

    return server

def udp_thread_target(udp_socket):
    while sta_globals['udp_run']:
        d = udp_socket.recvfrom(1024)
        data = d[0]
        addr = d[1]

        if not data:
            break

        handle_udp_data(data, addr)

        reply = 'ACK'
        udp_socket.sendto(reply , addr)
        log_debug('[UDP] Message [' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip(), prefix='udp')
        log_debug('', prefix='udp')

    log_debug('[UDP] Shutting down UDP server', prefix='udp')
    udp_socket.close()


def handle_udp_data(data, addr):
    split_data = data.split(';')
    if len(split_data) <= 1:
        return
    match_id = (int(split_data[0]) - 1)
    player_id = (int(split_data[1]) - 1)
    action = split_data[2]
    value = split_data[3]

    my_match = get_and_lock_matches()[match_id]
    release_matches()

    my_match.players[player_id].append({action: value})
