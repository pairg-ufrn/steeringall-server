from staruntime import sta_globals, get_and_lock_matches, release_matches
import json

class StaServerCommand(object):

    def __init__(self, identifier):
        self.command_id = identifier

    def call_command(self, command_id, command_body):
        if self.command_id == command_id:
            self.command(command_body)

    def command(self, command_body):
        pass


class StopServerCommand(StaServerCommand):

    def __init__(self):
        super(StopServerCommand, self).__init__('stop')

    def command(self, command_body):
        sta_globals['command_run'] = False


class QueryMatchesCommand(StaServerCommand):

    def __init__(self):
        super(QueryMatchesCommand, self).__init__('matches')

    def command(self, command_body):
        matches_count = len(get_and_lock_matches())
        print "=> Total de partidas ativas registradas: %d" % matches_count
        release_matches()

class MatchInfosCommand(StaServerCommand):

    def __init__(self):
        super(MatchInfosCommand, self).__init__('matchinfo')

    def command(self, command_body):
        match_id = (int(command_body[1]) - 1)
        my_match = get_and_lock_matches()[match_id]
        release_matches()

        print json.dumps(my_match.players)
