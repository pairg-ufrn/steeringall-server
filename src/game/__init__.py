class StaMatch(object):

    def __init__(self):
        self.id = None
        self.players = []
        self.mode = None

    def is_enabled(self):
        return len(self.players) == 2

    def connect(self):
        player_id = None
        if not self.is_enabled():
            self.players.append([])
            player_id = len(self.players)
        else:
            player_id = -1

        return player_id
