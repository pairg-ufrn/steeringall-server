import ConfigParser

def get_configuration(config_file):
    config = ConfigParser.RawConfigParser()
    config.read(config_file)

    return config


class STAConfiguration(object):

    def __init__(self, cfg_file):
        self.local_configs = {}
        self.file_configs = get_configuration(cfg_file)

    def set_config(self, key, value):
        self.local_configs.update({key: value})

    def get_config(self, key):
        return self.local_configs[key]

    def get_file_config(self):
        return self.file_configs
